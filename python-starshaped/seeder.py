import numpy as np
import bridson


def random_seeds(shape, dist):
    num = np.round(shape[0] * shape[1] / (dist ** 2)).astype(int)
    return np.random.rand(num, 2) * shape


def triangle_seeds(shape, dist):
    dx = dist
    dy = dist * np.sqrt(3 / 4)

    xs1 = list(np.arange(dx / 4, shape[1], dx))
    xs2 = list(np.arange(3 * dx / 4, shape[1], dx))
    ys = list(np.arange(dy / 2, shape[0], dy))

    seeds = []
    for yi, y in enumerate(ys):
        xs = xs1 if yi % 2 == 0 else xs2
        seeds += [[y, x] for x in xs]

    return np.array(seeds)


def square_seeds(shape, dist):
    xs = list(np.arange(dist / 2, shape[1], dist))
    ys = list(np.arange(dist / 2, shape[0], dist))
    return np.array([[y, x] for y in ys for x in xs])


def hex_seeds(shape, dist):
    dx = dist
    dy = dist * np.sqrt(3 / 4)

    xs1 = np.arange(dx / 4, shape[1], dx)
    xs2 = np.arange(3 * dx / 4, shape[1], dx)

    m1 = ([True, False, True] * np.ceil(len(xs1) / 3).astype(int))[0:len(xs1)]
    m2 = ([True, True, False] * np.ceil(len(xs2) / 3).astype(int))[0:len(xs2)]

    xs1 = list(xs1[m1])
    xs2 = list(xs2[m2])
    ys = list(np.arange(dy / 2, shape[0], dy))

    seeds = []
    for yi, y in enumerate(ys):
        xs = xs1 if yi % 2 == 0 else xs2
        seeds += [[y, x] for x in xs]

    return np.array(seeds)


def poisson_seeds(shape, dist):
    seeds = bridson.poisson_disc_samples(width=shape[1], height=shape[0], r=dist)
    return np.array(seeds)
