import numpy as np
import matplotlib.pyplot as plt


class StarShapedMetric:
    inner_radius = NotImplemented
    outer_radius = NotImplemented

    def dist(self, origin, targets, delta_angle):
        delta = targets - origin
        norms = np.hypot(*delta.T)
        angles = np.arctan2(*delta.T)

        return norms / self.angular_fun(angles + delta_angle)

    def angular_fun(self, angles):
        raise NotImplementedError()

    def plot(self):
        t = np.linspace(-2*np.pi, 0, 1000)
        r = self.angular_fun(t)
        plt.polar(t, r)
        plt.polar(t, np.full_like(t, self.inner_radius))
        plt.polar(t, np.full_like(t, self.outer_radius))
        plt.show()

    def closest_seeds(self, coords, seeds, considered_seeds=None):
        ncoords = len(coords)

        if considered_seeds is None:
            considered_seeds = range(len(seeds))

        if len(considered_seeds) == 1:
            return np.full(ncoords, considered_seeds[0])

        d_to_closest = np.full(ncoords, np.inf)
        closest_seed = np.empty(ncoords, dtype=int)

        for i, seed in enumerate(seeds):
            if i not in considered_seeds:
                continue

            d_to_seed = self.dist(seed[0:2], coords, seed[2])
            is_closer = d_to_seed < d_to_closest

            d_to_closest[is_closer] = d_to_seed[is_closer]
            closest_seed[is_closer] = i

        return closest_seed


class LinearStarShapedMetric(StarShapedMetric):
    def __init__(self, points):
        points_angles = np.arctan2(*points.T)
        points_radius = np.hypot(*points.T)

        directions = np.diff(points, axis=0, append=[points[0]])
        dot_points_dir = np.sum(points * directions, axis=-1, keepdims=True)
        dir_norm2 = np.sum(directions * directions, axis=-1, keepdims=True)

        t = - dot_points_dir / dir_norm2
        closest_point = points + t * directions
        segments_dist = np.hypot(*closest_point.T)
        segments_phi = np.arctan2(*closest_point.T)

        # Find the closest and the farthest distances of the metric function to the origin.
        # These points are either the endpoints of the segments, or the closest point of the line defining each
        # segments, if this point is located inbetween its endpoints.
        closest_point_in_segment = np.logical_and(np.less_equal(0, t), np.less_equal(t, 1)).flatten()
        considered_radiuses = np.concatenate((points_radius, segments_dist[closest_point_in_segment]))
        self.inner_radius = considered_radiuses.min()
        self.outer_radius = considered_radiuses.max()

        # We shift the data such that they are stored in order of increasing angle.
        shift = -np.argmin(points_angles)
        self._points_angles = np.roll(points_angles, shift)
        self._segments_dist = np.roll(segments_dist, shift)
        self._segments_phi = np.roll(segments_phi, shift)

        assert(np.all(np.diff(self._points_angles) >= 0))

    def angular_fun(self, angles):
        # Ensures that the angles are between -pi and pi, the output range of atan2.
        angles %= 2 * np.pi
        angles[angles > np.pi] -= 2 * np.pi

        # Polar equation of a line: http://www.nabla.hr/Z_MemoHU-015.htm
        indices = (np.searchsorted(self._points_angles, angles) - 1) % len(self._points_angles)
        return self._segments_dist[indices] / np.cos(angles - self._segments_phi[indices])

    @classmethod
    def from_file(cls, filename):
        points = np.loadtxt(filename, delimiter=',')
        points = np.roll(points, -1, axis=-1)   # Respect the convention that coordinates should be in y, x format.
        return cls(points)


class EuclideanMetric(StarShapedMetric):
    inner_radius = 1
    outer_radius = 1

    def angular_fun(self, angles):
        return np.ones_like(angles)
