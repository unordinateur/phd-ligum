This code essentially re-implements the code from https://hal.inria.fr/hal-02118846/file/paper.pdf. The main goal of this implementation was to understand the approach well and debug my code against known results, before extending my approach to the surface of 3D models.

However, instead of using a "growth model" of the Voronoï cells, it instead does a "static" assignation of which cell each pixel of the image tentatively belongs too. It then identifies the "wrong" pixels (those who are isolated from their seed) and fixes them by performing a new search for the closest seed, but this time only considering the seeds of the contiguous regions which do contain their seed.

This approach is much faster, and produces comparable (but not identical) results. In particular, the growth model approach guarantees centrally convex cells, whereas my approach offers no such guarantee. (I still consider the HUGE speed gain an improvement.)

Another improvement is that my implementation does not require that the seeds be disposed regularly. They can also be placed randomly. "seeder.py" contains the various dispositions I implemented: square, triangle and hex regular patterns; and uniform and poisson-distributed random distributions.

"metric.py" contains the code that implements the starshaped metric themselves. It also contains an EuclideanMetric to use as a sanity check. I only implemented linearly-interpolated metrics, and not polynomially-interpolated metrics.

"util.py" contains helper functions.

POSSIBLE IMPROVEMENT: The computations are performed in raster space, just like the code in the original paper. However the paper mentions a proof that if the starshaped metric is bound by straight segments, the resulting Voronoï partition of space would also be bound by straight line segments. It should be possible to directly compute the intersection point of each cells, and thus create the voronoï partition directly in vector space. That would be a huge improvement, because then the precision/quality of the computations would not be bound by the resolution of the output image. It would also probably be faster, especially for large resolution outputs.

The program takes a few second to run, with no visible output until the end.
