import numpy as np
import skimage


def shifted_slice(shift, start, stop):
    return slice(start + max(0, shift), stop + min(0, shift))


def detect_edges(image, radius):
    h, w = image.shape
    out = np.zeros_like(image, dtype=np.bool)
    for y, x in zip(*skimage.draw.circle(0, 0, radius + 0.5)):
        s1 = (shifted_slice(y, 0, h), shifted_slice(x, 0, w))
        s2 = (shifted_slice(-y, 0, h), shifted_slice(-x, 0, w))
        out[s2] |= image[s1] != image[s2]

    return out


def neighbour_pixels(image):
    h, w = image.shape

    edge = np.zeros_like(image, dtype=np.bool)
    for y, x in ((-1, 0), (1, 0), (0, -1), (0, 1)):
        s1 = (shifted_slice(y, 0, h), shifted_slice(x, 0, w))
        s2 = (shifted_slice(-y, 0, h), shifted_slice(-x, 0, w))
        edge[s2] |= image[s1] != image[s2]

    return edge & (~image)
