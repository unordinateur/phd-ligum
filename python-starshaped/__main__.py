import time

import numpy as np
import matplotlib.pyplot as plt
import skimage
import astar

import seeder
import util
from metric import EuclideanMetric, LinearStarShapedMetric

shape = (500, 500)  # height, width
adist = 50
edge_radius = 1
star_file = 'triangular_threeaxisreflection_polygon.txt'


# metric = EuclideanMetric()
metric = LinearStarShapedMetric.from_file(star_file)
# metric.plot()

shape_extension = np.ceil(adist * metric.outer_radius / metric.inner_radius).astype(np.int)
extended_shape = np.array(shape) + 2 * shape_extension

seeder_fun = (
    seeder.square_seeds,
    seeder.triangle_seeds,
    seeder.hex_seeds,
    seeder.random_seeds,
    seeder.poisson_seeds
)
seeds = seeder_fun[-1](extended_shape, adist)
seeds -= shape_extension

# Add an angle to the seeds
randomize_angles = True
if randomize_angles:
    seeds_angles = np.random.rand(len(seeds), 1) * 2 * np.pi
else:
    seeds_angles = np.zeros((len(seeds), 1))
seeds = np.hstack((seeds, seeds_angles))

# Find to which cell each pixel belongs
coords = np.indices(shape).reshape(2, -1).T
closest_seed = metric.closest_seeds(coords, seeds)
closest_seed.shape = shape

# Fix the result such that the domain of each seed is simply connected.
#   Input: A picture where each pixel's value is the ID of the closest seed according to the metric.
#   1. Enumerate all simply connected regions of the picture.
#   2. For each region, check if it contains its seed. If not, it needs fixing.
#       - If the seed is in the picture
#           a. For each region, find its associated seed.
#           b. Find the region in which is located the seed.
#           c. If the region of the seed is the same than the current region, then the region contains the seed.
#       - If the seed is outside the picture:
#           - If the region is fully enclaved into the picture, then the region does not contain its seed.
#           - Else, we can't tell. FIXME: We don't fix the region for now.
#   3. Merge together regions that need fixing that touch each other.
#   4. Re-apply the `closest_seeds` method on the pixels located in each of the merged regions that need fixing,
#      limiting the search only to the seeds of neighboring "good" regions.
#       - If nothing change, re-apply the change excluding from each pixel their current seed.
#   5. Repeat until no region needs fixing.

while True:
    regions = skimage.measure.label(closest_seed, connectivity=1, background=-1)

    to_fix = np.zeros_like(closest_seed)
    for region_props in skimage.measure.regionprops(regions):
        coords = region_props.coords
        point_in_region = tuple(coords[0])
        seed = closest_seed[point_in_region]
        seed_pos = seeds[seed][0:2]

        seed_pos_int = tuple(seed_pos.astype(int))
        if (0 <= seed_pos_int[0] < shape[0]) and (0 <= seed_pos_int[1] < shape[1]):
            # The seed is in the screen. We need to fix the region if it is in another region than the current one.
            fix_region = (regions[seed_pos_int] != region_props.label)
        else:
            # The seed is not visible
            if np.all(np.not_equal(region_props.bbox, (0, 0, *shape))):
                # The region does not touch the boundary of the image. Therefore it is totally enclaved in the image,
                # which guarantees that the seed is in another region. Fix the region!
                fix_region = True
            else:
                # The region touches the boundary of the shape.
                # Draw a line from the seed to its closest point in the region.
                boundary_mask = ((coords[:, 0] == 0) | (coords[:, 0] == (shape[0] - 1))
                                 | (coords[:, 1] == 0) | (coords[:, 1] == (shape[1] - 1)))
                boundary_coords = coords[boundary_mask]
                delta = boundary_coords - seed_pos
                dist_to_seed = np.hypot(*delta.T)

                shortest_i = np.argmin(dist_to_seed)
                shortest_delta = delta[shortest_i]
                shortest_dist = dist_to_seed[shortest_i]

                test_coords = seed_pos + np.outer(np.linspace(0, 1, shortest_dist.astype(int)), shortest_delta)
                test_seeds = metric.closest_seeds(test_coords, seeds)

                if np.all(test_seeds == seed):
                    # The line is fully contained within the seed's domain: The regions touches its seed! No fixes
                    # needed.
                    fix_region = False
                else:
                    # The line crosses the domain boundary. The region -might- need fixing. At this point we need to do
                    # a full generation of the hidden region; OR use pathfinding. FIXME: For now, we don't fix.
                    fix_region = False
                    """
                    excluded_seeds = [csh[point_in_region] for csh in closest_seed_history[:-1]]
    
                    def neighbors(n):
                        test_coords = (
                            (n[0] - 1, n[1]),
                            (n[0] + 1, n[1]),
                            (n[0], n[1] - 1),
                            (n[0], n[1] + 1),
                        )
                        test_closest_seeds = metric.closest_seeds(test_coords, seeds, excluded_seeds)
                        return (tc for tc, tcs in zip(test_coords, test_closest_seeds) if tcs == seed)
    
                    def heuristic(n, goal):
                        delta = np.subtract(goal, n)
                        return np.hypot(*delta)
    
                    path = astar.find_path(point_in_region, tuple(seed_pos),
                                           neighbors_fnct=neighbors,
                                           heuristic_cost_estimate_fnct=heuristic)
    
                    fix_region = (path is None)
                    """

        if fix_region:
            to_fix[coords[:, 0], coords[:, 1]] = 1

    # We now have identified all pixels that need fixing. List individually all the contiguous regions.
    fix_regions = skimage.measure.label(to_fix, connectivity=1)
    regions_props = skimage.measure.regionprops(fix_regions)
    fixed_region = False
    for region_props in regions_props:
        label = region_props.label
        if label == 0:
            # Region 0 is the background: it should be ignored.
            continue

        # Recompute the closest seeds of the region needing fixing, but limiting the search to the seeds of the
        # neighboring regions.
        edges = util.neighbour_pixels(fix_regions == label)
        neighbour_seeds = set(closest_seed[edges])

        coords = region_props.coords
        new_closest_seed = metric.closest_seeds(coords, seeds, list(neighbour_seeds))

        # If this changes nothing, repeat by excluding from the search each pixel's current seed.
        if np.all(closest_seed[(*coords.T,)] == new_closest_seed):
            region_seeds = set(closest_seed[(*coords.T,)])
            for s in region_seeds:
                mask = (new_closest_seed == s)
                new_closest_seed[mask] = metric.closest_seeds(coords[mask], seeds, list(neighbour_seeds - {s}))

        closest_seed[(*coords.T,)] = new_closest_seed
        fixed_region = True

    if not fixed_region:
        break

plt.imshow(closest_seed)
plt.scatter(seeds[:, 1], seeds[:, 0])
plt.show()

# Show result
out = util.detect_edges(closest_seed, edge_radius)
plt.imshow(out)
plt.scatter(seeds[:, 1], seeds[:, 0])
plt.show()
