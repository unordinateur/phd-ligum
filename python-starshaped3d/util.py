import numpy as np
import skimage


def detect_edges(image, radius=None):
    def shifted_slice(shift, start, stop):
        return slice(start + max(0, shift), stop + min(0, shift))

    h, w = image.shape

    if radius is None:
        shape = ((-1, 0), (1, 0), (0, -1), (0, 1))
    else:
        shape = zip(*skimage.draw.circle(0, 0, radius + 0.5))

    edges = np.zeros_like(image, dtype=np.bool)
    for y, x in shape:
        s1 = (shifted_slice(y, 0, h), shifted_slice(x, 0, w))
        s2 = (shifted_slice(-y, 0, h), shifted_slice(-x, 0, w))
        edges[s2] |= (image[s1] != image[s2])

    return edges


def normalize(v):
    return v / np.linalg.norm(v)


def scalar_triple_prod(a, b, c):
    c0 = b[:, 1]*c[:, 2] - b[:, 2]*c[:, 1]
    c1 = b[:, 2]*c[:, 0] - b[:, 0]*c[:, 2]
    c2 = b[:, 0]*c[:, 1] - b[:, 1]*c[:, 0]
    return a[:, 0]*c0 + a[:, 1]*c1 + a[:, 2]*c2
