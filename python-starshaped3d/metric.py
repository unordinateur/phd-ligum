import numpy as np
import matplotlib.pyplot as plt

import util


class StarShapedMetric:
    inner_radius = NotImplemented
    outer_radius = NotImplemented

    def dist(self, p1, n1, r1, p2, n2, geomode):

        # Considérer: Isophotic Metric
        delta = p2 - p1
        de = np.sqrt(np.sum(delta ** 2, axis=-1, keepdims=True))
        if geomode == 1:
            # https://www.microsoft.com/en-us/research/wp-content/uploads/2010/12/c95-f95_199-a16-paperfinal-v5.pdf
            v = delta / de
            c1 = np.sum(n1 * v, axis=-1)
            c2 = np.sum(n2 * v, axis=-1)

            div0 = np.abs(c1 - c2) < 0.001

            g = np.empty(len(p2), dtype=np.float)
            g[~div0] = (np.arcsin(c1[~div0]) - np.arcsin(c2[~div0])) / (c1[~div0] - c2[~div0])
            g[div0] = 1 / np.sqrt(1 - (c1[div0] + c2[div0]) ** 2 / 4)
        elif geomode == 2:
            # https://link.springer.com/content/pdf/10.1007%2Fs11432-011-4322-8.pdf
            b = 2
            dotnormals = np.sum(n1 * n2, axis=-1)
            g = 1 + ((1 - dotnormals)/2) ** b

        else:
            g = 1

        # Project delta onto seed surface
        delta -= n1 * np.sum(n1 * delta, axis=-1, keepdims=True)
        delta /= np.sqrt(np.sum(delta ** 2, axis=-1, keepdims=True))

        # Angle
        sintheta = util.scalar_triple_prod(*np.broadcast_arrays(n1, delta, r1))
        theta = np.arcsin(sintheta)



        return g * de.flatten() / self.angular_fun(theta)

    def angular_fun(self, angles):
        raise NotImplementedError()

    def plot(self):
        t = np.linspace(-2*np.pi, 0, 1000)
        r = self.angular_fun(t)
        plt.polar(t, r)
        plt.polar(t, np.full_like(t, self.inner_radius))
        plt.polar(t, np.full_like(t, self.outer_radius))
        plt.show()

    def closest_seeds(self, coords, c_normals, seeds, s_normals, s_norths, considered_seeds=None, geomode=0):
        ncoords = len(coords)

        if considered_seeds is None:
            considered_seeds = range(len(seeds))

        if len(considered_seeds) == 1:
            return np.full(ncoords, considered_seeds[0])

        d_to_closest = np.full(ncoords, np.inf)
        closest_seed = np.empty(ncoords, dtype=int)

        for i, (seed, s_normal, s_north) in enumerate(zip(seeds, s_normals, s_norths)):
            if i not in considered_seeds:
                continue

            d_to_seed = self.dist(seed, s_normal, s_north, coords, c_normals, geomode)
            is_closer = d_to_seed < d_to_closest

            d_to_closest[is_closer] = d_to_seed[is_closer]
            closest_seed[is_closer] = i

        return closest_seed


class LinearStarShapedMetric(StarShapedMetric):
    def __init__(self, points):
        points_angles = np.arctan2(*points.T)
        points_radius = np.hypot(*points.T)

        directions = np.diff(points, axis=0, append=[points[0]])
        dot_points_dir = np.sum(points * directions, axis=-1, keepdims=True)
        dir_norm2 = np.sum(directions * directions, axis=-1, keepdims=True)

        t = - dot_points_dir / dir_norm2
        closest_point = points + t * directions
        segments_dist = np.hypot(*closest_point.T)
        segments_phi = np.arctan2(*closest_point.T)

        # Find the closest and the farthest distances of the metric function to the origin.
        # These points are either the endpoints of the segments, or the closest point of the line defining each
        # segments, if this point is located inbetween its endpoints.
        closest_point_in_segment = np.logical_and(np.less_equal(0, t), np.less_equal(t, 1)).flatten()
        considered_radiuses = np.concatenate((points_radius, segments_dist[closest_point_in_segment]))
        self.inner_radius = considered_radiuses.min()
        self.outer_radius = considered_radiuses.max()

        # We shift the data such that they are stored in order of increasing angle.
        shift = -np.argmin(points_angles)
        self._points_angles = np.roll(points_angles, shift)
        self._segments_dist = np.roll(segments_dist, shift)
        self._segments_phi = np.roll(segments_phi, shift)

        assert(np.all(np.diff(self._points_angles) >= 0))

    def angular_fun(self, angles):
        # Ensures that the angles are between -pi and pi, the output range of atan2.
        angles %= 2 * np.pi
        angles[angles > np.pi] -= 2 * np.pi

        # Polar equation of a line: http://www.nabla.hr/Z_MemoHU-015.htm
        indices = (np.searchsorted(self._points_angles, angles) - 1) % len(self._points_angles)
        return self._segments_dist[indices] / np.cos(angles - self._segments_phi[indices])

    @classmethod
    def from_file(cls, filename):
        points = np.loadtxt(filename, delimiter=',')
        points = np.roll(points, -1, axis=-1)   # Respect the convention that coordinates should be in y, x format.
        return cls(points)


class EuclideanMetric(StarShapedMetric):
    inner_radius = 1
    outer_radius = 1

    def angular_fun(self, angles):
        return np.ones_like(angles)
