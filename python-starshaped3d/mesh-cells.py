import time

import numpy as np
import matplotlib.pyplot as plt
import trimesh.path.entities
import networkx as nx
from trimesh.path.entities import Line
from trimesh.path.path import Path3D

import util
from metric import EuclideanMetric, LinearStarShapedMetric

shape = (500, 500)  # height, width
adist = 50
edge_radius = 1
star_file = 'triangular_threeaxisreflection_polygon.txt'

# Load mesh
mesh0 = trimesh.load_mesh('model/bunny.off')
mesh = mesh0.copy()

# Refine the mesh
for _ in range(2):
    mesh = trimesh.Trimesh(*trimesh.remesh.subdivide(mesh.vertices, mesh.faces))

faces = mesh.faces
vertices = mesh.vertices
normals = mesh.face_normals
graph = nx.from_edgelist(mesh.face_adjacency)


# Precompute face basis and centers
faces_basis = np.empty((len(faces), 2, 3))
faces_centers = np.empty((len(faces), 3))
for i, face_vertices in enumerate(faces):
    xhat = util.normalize(np.diff(vertices[face_vertices[0:2]], axis=0))
    xhat /= np.linalg.norm(xhat)
    yhat = np.cross(normals[i], xhat)

    faces_basis[i] = [xhat, yhat]
    faces_centers[i] = np.mean(vertices[face_vertices], axis=0)

seeds, seeds_faces = trimesh.sample.sample_surface_even(mesh, 200)
seeds_normals = normals[seeds_faces]
seeds_ref_dir = faces_basis[seeds_faces, 0]
seeds_visual = trimesh.points.PointCloud(seeds, color=[255, 0, 0, 255])

metric = LinearStarShapedMetric.from_file(star_file)

# Find to which cell each pixel belongs
closest_seed = metric.closest_seeds(faces_centers, normals, seeds, seeds_normals, seeds_ref_dir, geomode=2)

# Fix the result such that the domain of each seed is simply connected.
#   Input: A graph with each node is annotated with an associated seed.
#   1. Enumerate all simply connected regions of the graph.
#   2. For each region, check if it contains its seed. If not, it needs fixing.
#   3. Merge together regions that need fixing that touch each other.
#   4. Re-apply the `closest_seeds` method on the pixels located in each of the merged regions that need fixing,
#      limiting the search only to the seeds of neighboring "good" regions.
#       - If nothing change, re-apply the change excluding from each pixel their current seed.
#   5. Repeat until no region needs fixing.
while True:
    # Make a graph of the faces, but remove all edges linking faces not associated with the same seed.
    # Each connected component will thus share the same seed.
    graph_seeds = nx.from_edgelist(pair for pair in mesh.face_adjacency if closest_seed[pair[0]] == closest_seed[pair[1]])

    # Mark as "ok" all the faces in the same connected component as the face containing seeds
    to_fix = np.ones_like(closest_seed)
    for seed_face in seeds_faces:
        seed_patch_faces = list(nx.node_connected_component(graph_seeds, seed_face))
        to_fix[seed_patch_faces] = False

    print(sum(to_fix))

    # Make a graph of all the faces that are not ok: each connected component of that graph is a patch that need fixing.
    graph_patches = nx.from_edgelist(mesh.face_adjacency)
    graph_patches.remove_nodes_from(i for i, tf in enumerate(to_fix) if not tf)

    # For each connected component of that graph...
    fixed_region = False
    while graph_patches.nodes:
        # List its nodes, and remove them from the graph, since they are all processed together.
        patch_nodes = list(nx.node_connected_component(graph_patches, list(graph_patches.nodes)[0]))
        graph_patches.remove_nodes_from(patch_nodes)
        fixed_region = True

        # List all the neighboring faces in the original graph
        patch_neighbours = set()
        for node in patch_nodes:
            patch_neighbours |= set(graph.neighbors(node))
        patch_neighbours -= set(patch_nodes)

        # Find the seed associated to those faces
        neighbour_seeds = set()
        for neighbour in patch_neighbours:
            neighbour_seeds.add(closest_seed[neighbour])

        # And fix the patch faces by recomputing their closest seed, considering only the neighboring seeds.
        new_closest_seed = metric.closest_seeds(
                                        faces_centers[patch_nodes], normals[patch_nodes],
                                        seeds, seeds_normals, seeds_ref_dir,
                                        considered_seeds=list(neighbour_seeds),
                                        geomode=2
                                    )

        # If this changes nothing, repeat by excluding from the search each face's current seed.
        cur_closest_seed = closest_seed[patch_nodes]
        if np.all(cur_closest_seed == new_closest_seed):
            region_seeds = set(new_closest_seed)
            for s in region_seeds:
                mask = (cur_closest_seed == s)
                new_closest_seed[mask] = metric.closest_seeds(
                                            faces_centers[patch_nodes][mask], normals[patch_nodes][mask],
                                            seeds, seeds_normals, seeds_ref_dir,
                                            considered_seeds=list(neighbour_seeds - {s}),
                                            geomode=2
                                         )

        closest_seed[patch_nodes] = new_closest_seed

    # If no regions were fixed, we are done!
    if not fixed_region:
        break

# Get the outline of each region.
cell_edges = []
for f1, f2 in mesh.face_adjacency:
    if closest_seed[f1] != closest_seed[f2]:
        edge = set(mesh.faces[f1]) & set(mesh.faces[f2])
        cell_edges.append(Line(list(edge)))

vertices = np.copy(mesh.vertices)

for face, normal in zip(mesh.faces, mesh.face_normals):
    vertices[face] += normal*0.00001

outline = Path3D(cell_edges, vertices)

scene = trimesh.Scene([
    mesh0,
    seeds_visual,
    outline
])

scene.show(smooth=False)

exit()


# Show result
out = util.detect_edges(closest_seed, edge_radius)
plt.imshow(out)
plt.scatter(seeds[:, 1], seeds[:, 0])
plt.show()
