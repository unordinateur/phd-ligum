import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

import trimesh
import skimage
import PIL

from metric import LinearStarShapedMetric
from util import detect_edges, normalize

# PARAMETERS
mesh_file = 'model/bunny.obj'
metric_type = LinearStarShapedMetric
star_file = 'triangular_threeaxisreflection_polygon.txt'
tex_shape = (2048, 2048)
num_seeds = 200

# LOAD MESH
print("Loading mesh")

mesh = trimesh.load_mesh(mesh_file)
metric = metric_type.from_file(star_file)

# Precompute useful data
faces = mesh.faces              # Vertices index associated with each face
vertices = mesh.vertices        # Coordinates of each vertex.
normals = mesh.face_normals     # Normal vector of each face

# Face_graph: A graph where each node is a face of the mesh, and they share an edge if they touch.
# We can't use the original mesh to construct this graph, because triangles that touch in reality might not use the
# same vertices indices, if they have different UV coordinates (even if they have the same XYZ coordinates). So we
# create a temporary copy that discards the texture information.
mesh_temp = mesh.copy()
mesh_temp.merge_vertices(textured=False)
faces_graph = nx.from_edgelist(mesh_temp.face_adjacency)

# faces_basis: Pair of orthogonal vectors parallel to each face.
faces_basis = np.empty((len(faces), 2, 3))
for i, face_vertices in enumerate(faces):
    xhat = normalize(np.cross([0, 1, 0], normals[i]))
    yhat = np.cross(normals[i], xhat)

    faces_basis[i] = [xhat, yhat]

# COMPUTE TEXTURE COORDINATES
print("Computing texture coordinates")

# Each vertex has its associated UV coordinates in the texture. We fix the representation used by trimesh, to match
# the one used by numpy.
tex_uv = mesh.visual.uv[:, ::-1].copy()
tex_uv[:, 0] = 1 - tex_uv[:, 0]
tex_uv *= tex_shape

# For each UV coordinate, we compute to which face it's associated, its corresponding 3D (xyz) position, and the 3D
# normal of that point on the mesh. These computations requires in an intermediate step, for each face, the
# computation of a matrix. We cache the matrices so we can reuse them later.
tex_faces = np.full(tex_shape, -1, dtype=np.int)
tex_xyz = np.empty((*tex_shape, 3), dtype=np.float)
tex_normals = np.empty((*tex_shape, 3), dtype=np.float)

faces_mat = np.empty((len(faces), 3, 3), dtype=np.float)

for i, f in enumerate(faces):
    vertex_uvs = tex_uv[f]
    uvmat = np.column_stack((vertex_uvs, (1, 1, 1)))
    uvmatinv = np.linalg.inv(uvmat)
    faces_mat[i] = fmat = np.matmul(uvmatinv, vertices[f])

    tri_uvs = skimage.draw.polygon(*vertex_uvs.T)
    vecs = np.column_stack((*tri_uvs, np.ones(len(tri_uvs[0]))))
    tex_faces[tri_uvs] = i
    tex_xyz[tri_uvs] = np.matmul(vecs, fmat)
    tex_normals[tri_uvs] = normals[i]

tex_covered = (tex_faces != -1)  # True for each pixel which is visible on the mesh.



# COMPUTE SEEDS
print("Computing seeds")

# Randomly pick semi-uniform (poisson-disk distributed) seeds on the surface
seeds, seeds_faces = trimesh.sample.sample_surface_even(mesh, num_seeds)

# Find their associated normal and "up" direction
seeds_normals = normals[seeds_faces]
seeds_ref_dir = faces_basis[seeds_faces, 1]

# Randomly associate a color to each seed
seed_colors = (np.random.rand(len(seeds), 3) * 256).astype(np.uint8)

# Find the UV coordinates associated to each seed.
# - First, we compute each seed's barycentriuc coordinate in their associated face
# - From that, we can find the seed's UV coordinate
# - We pick the nearest integer-valued coordinate that is in a "covered" pixel. (We can't simply round the coordinates
#   to the nearest integer-valued coordinate, because it might fall into an uncovered pixel. In theory this should not
#   happen, but if a seed is close enough a texture seam, it might happen because of a rounding error.)
seeds_bary = trimesh.triangles.points_to_barycentric(vertices[faces[seeds_faces]], seeds)
seeds_uv_int = np.zeros((len(seeds), 2), dtype=np.int)
for i, (sb, sf) in enumerate(zip(seeds_bary, seeds_faces)):
    seed_uv = np.matmul(sb, tex_uv[faces[sf]])

    rounded_u, rounded_v = np.transpose((np.floor(seed_uv), np.ceil(seed_uv)))
    test_coords = np.array([(ru, rv) for ru in rounded_u for rv in rounded_v], dtype=np.int)
    mask = tex_covered[tuple(test_coords.T)]
    test_coords = test_coords[mask]

    delta = test_coords - seed_uv
    dist = np.sum(delta ** 2, axis=-1)
    imin = np.argmin(dist)
    seeds_uv_int[i] = test_coords[imin]

# FLATTEN DATA
print("Flattening data")

# We make 1D lists of the relevant data of all the covered pixels.
# That means that each covered pixel now becomes uniquely designated by a single integer, and all the data structures
# only contain covered pixel. This makes indexing and processing the data easier down the line.

work_indices = np.empty(tex_shape, dtype=np.int)    # The '1D' index associated with each original pixel.
work_indices[tex_covered] = np.arange(np.sum(tex_covered))

work_xyz = tex_xyz[tex_covered]
work_normals = tex_normals[tex_covered]
work_seeds_uv = work_indices[tuple(seeds_uv_int.T)]

# COMPUTE INITIAL SEED DISTRIBUTION
print("Computing initial closest seeds to each pixel")

# Compute the closest seed to each "covered" pixel, according to the metric. At this point, there will be isolated
# islands: We are going to fix those in the next steps.
work_closest_seed = metric.closest_seeds(work_xyz, work_normals, seeds, seeds_normals, seeds_ref_dir, geomode=2)

# CREATE PIXEL ADJACENCY GRAPH
print("Creating pixel adjacency graph")

import time
start = time.time()

# Create a graph where each node represents a pixel in the texture, with an edge between each pixel that touches.
# For most pixels, this is straightforward: take its 4 neighbors. However, if a pixel touches a non-covered pixel,
# that means that this pixel touches a texture seam. Its neighboring pixels are therefore somewhere else in the
# texture!

# If we fleshed out this graph in full, it could become enormous, with O(n^2) nodes and twice as many edges! Therefore,
# we will only consider a subset of the true edges, knowing that later in this algorithm, we are going to prune this
# graph anyway: no need to include nodes which we are going to remove immediately!

# The pruning process replaces all nodes which represent pixels that are in patches that touch their seed, with a
# single node that is the seed's pixel. Therefore, we only need to include in our graph nodes that are in patches that
# do NOT touch their seed, and the seed's nodes. (As for the edges, we need to include all the edges that would be
# included had we constructed the full graph and then pruned it.)

# Return the computed initial seed distribution into its "true" 2D image: it's necessary to compute neighborhoods.
tex_closest_seed = np.full(tex_shape, -1, dtype=np.int)
tex_closest_seed[tex_covered] = work_closest_seed

# Label `tex_closest_seed`: Each contiguous region of pixels of a single color gets a unique index (its label).
tex_labeled = skimage.measure.label(tex_closest_seed, background=-1, connectivity=1)

# For each seed, find the label of the region in which it resides, and mark all pixel that shares any seed's label.
# Any unmarked pixel thus resides in a patch that is disconnected from their seed.
seed_regions = tex_labeled[tuple(seeds_uv_int.T)]
tex_seed_in_patch = np.zeros(tex_shape, dtype=np.bool)
for sr in seed_regions:
    tex_seed_in_patch |= (tex_labeled == sr)

# Precompute some useful data
boundary_pixels = detect_edges(tex_covered)
inner_boundary = boundary_pixels & tex_covered  # Covered pixels that touch a texture seam
outer_boudary = boundary_pixels & ~tex_covered  # Uncovered pixels that touch a texture seam

boundary_faces = tex_faces[inner_boundary]  # Faces of the inner boundary pixels
boundary_xyz = tex_xyz[inner_boundary]      # XYZ position of the inner boundary pixels
boundary_uv = np.argwhere(inner_boundary)   # UV position of the inner_boundary pixels

# Pixels in `inner_boundary` touch pixels in `outer_boundary`: There should be an edge between those two pixels, but
# since `outer_boundary` is not covered, we instead map its pixels into other covered pixels. We keep note of these
# mappings here.
boundary_pairs = []


# Given an node's 2D position, return its 1D index. If the node is in a patch that contains a seed, instead returns
# that seed's node index.
def relabel_node(node):
    if tex_seed_in_patch[node]:
        seed = tex_closest_seed[node]
        seed_uv_int = seeds_uv_int[seed]
        node = tuple(seed_uv_int)

    return work_indices[node]


# Add an edge to the graph between two "covered" nodes.
def add_internal_edge(graph, anode, bnode):
    anode = relabel_node(anode)
    bnode = relabel_node(bnode)
    graph.add_edge(anode, bnode)

# Add an edge between a "covered" node, and its node across a seam.
def add_boundary_edge(graph, in_node, out_node):
    # Assume that the "outside" pixel was in the same plane/metric than the "inside" pixels's triangle: What would be
    # its 3D position? (It is, of course, outside the triangle, but we can extrapolate linearly)
    face_i = tex_faces[in_node]
    fmat = faces_mat[face_i]
    vec = (*out_node, 1)
    xyz = np.matmul(vec, fmat)

    # Find the closest "covered" node to the extrapolated position, provided that that node is also
    # on a boundary, and is on a triangle adjacent to the current triangle.
    # Fixme: do not match pairs across gaps! (Right now, if a pixel is nearby a gap, and has no neighbors, it is
    #        matched instead to a the closest pixel on a sideways triangle; this can intoduce artifacts nearby gaps.
    mask = np.zeros_like(boundary_faces, np.bool)
    for face_j in faces_graph.neighbors(face_i):
        mask |= (boundary_faces == face_j)

    if mask.any():
        delta = boundary_xyz[mask] - xyz
        dist = np.sum(delta ** 2, axis=-1)
        imin = np.argmin(dist)

        # Add an edge between the first node and the found node, and keep a not of the mapping between the original
        # "outside" second pixel, and the one we founde.
        fixed_node = tuple(boundary_uv[mask][imin])
        add_internal_edge(graph, in_node, fixed_node)
        boundary_pairs.append((out_node, fixed_node))


# Main graph creation loop. We consider horizontal edges first, then vertical edges.
work_graph = nx.Graph()
for dy, dx in ((0, 1), (1, 0)):
    s1 = slice(None, tex_shape[0] - dy), slice(None, tex_shape[1] - dx)
    s2 = slice(dy, None), slice(dx, None)

    # We build an edge between 2 pixels if they are neighbor and :
    # - both of them are in patches that do not contain a seed
    # - OR each of them is assigned a different seed (so the edge we create that indicate that two regions touch,
    #   even if the nodes themselves might be replaced by `relabel_node`.)
    # - AND both pixels are covered.
    internal_edges = (
        ~(tex_seed_in_patch[s1] | tex_seed_in_patch[s2])
        | (tex_closest_seed[s1] != tex_closest_seed[s2])
    ) & tex_covered[s1] & tex_covered[s2]

    for y, x in np.argwhere(internal_edges):
        add_internal_edge(work_graph, (y, x), (y + dy, x + dx))

    # We also add edges between pairs of contiguous pixels when one of them is in `inner_boundary` and the other is
    # in `outer_boudary`, but here we must do the pixels mapping across seams.
    for y, x in np.argwhere(inner_boundary[s1] & outer_boudary[s2]):
        # When the "outer" pixel is to the bottom/right
        add_boundary_edge(work_graph, (y, x), (y + dy, x + dx))

    for y, x in np.argwhere(outer_boudary[s1] & inner_boundary[s2]):
        # When the "outer" pixel is to the top/left
        add_boundary_edge(work_graph, (y + dy, x + dx), (y, x))

stop = time.time()
print(stop-start)

# Find to which cell each pixel belongs
print("Fixing closest seeds to each pixel")

# Fix the result such that the domain of each seed is simply connected.
#   Input: A graph with each node is annotated with an associated seed.
#   1. Enumerate all simply connected regions of the graph.
#   2. For each region, check if it contains its seed. If not, it needs fixing.
#   3. Merge together regions that need fixing that touch each other.
#   4. Re-apply the `closest_seeds` method on the pixels located in each of the merged regions that need fixing,
#      limiting the search only to the seeds of neighboring "good" regions.
#       - If nothing change, re-apply the change excluding from each pixel their current seed.
#   5. Repeat until no region needs fixing.
while True:
    # Make a graph of the faces, but remove all edges linking faces not associated with the same seed.
    # Each connected component will thus share the same seed.
    graph_seeds = nx.from_edgelist(
        edge for edge in work_graph.edges if work_closest_seed[edge[0]] == work_closest_seed[edge[1]]
    )

    # Prune the pixel graph: Collapse all the pixels that don't need fixing into a single node, the seed node.
    for seed_pixel in work_seeds_uv:
        if seed_pixel in graph_seeds:
            seed_patch_pixels = nx.node_connected_component(graph_seeds, seed_pixel)
            nx.relabel_nodes(work_graph, {px: seed_pixel for px in seed_patch_pixels}, copy=False)

    # Make a graph of all the pixels that need fixing: They are all the pixels left in `work_graph`, except the pixels
    # that are seeds. Removing those nodes will split the graph in several disconnected components, each being a single
    # isolated "island" that need fixing.
    tofix_graph = work_graph.copy()
    tofix_graph.remove_nodes_from(work_seeds_uv)

    # If `tofix_graph` is empty, we are done!
    if len(tofix_graph.nodes) == 0:
        break

    # For each connected component of that graph...
    for patch_nodes in nx.connected_components(tofix_graph):
        # List all the neighboring pixels in the original graph
        patch_neighbours = set()
        for pn in patch_nodes:
            patch_neighbours |= set(work_graph.neighbors(pn))
        patch_neighbours -= patch_nodes
        patch_nodes = list(patch_nodes)

        # Find the seed associated to those pixels
        neighbour_seeds = set()
        for neighbour in patch_neighbours:
            neighbour_seeds.add(work_closest_seed[neighbour])

        # And fix the patch faces by recomputing their closest seed, considering only the neighboring seeds.
        new_closest_seed = metric.closest_seeds(
                                        work_xyz[patch_nodes], work_normals[patch_nodes],
                                        seeds, seeds_normals, seeds_ref_dir,
                                        considered_seeds=list(neighbour_seeds),
                                        geomode=2
                                    )

        # If this changes nothing, repeat by excluding from the search each pixels's current seed.
        cur_closest_seed = work_closest_seed[patch_nodes]
        if np.all(cur_closest_seed == new_closest_seed):
            region_seeds = set(new_closest_seed)
            for s in region_seeds:
                mask = (cur_closest_seed == s)
                new_closest_seed[mask] = metric.closest_seeds(
                                            work_xyz[patch_nodes][mask], work_normals[patch_nodes][mask],
                                            seeds, seeds_normals, seeds_ref_dir,
                                            considered_seeds=list(neighbour_seeds - {s}),
                                            geomode=2
                                         )

        work_closest_seed[patch_nodes] = new_closest_seed

print("Displaying result")

# Create the final version of 'tex_closest_seed': First copy back into the 2D structure the 1D work data
tex_closest_seed[tex_covered] = work_closest_seed

# Then, for each "outer" boundary pixel, copy the seed of it's mapped pixel (When rendering the mesh, the GPU uses
# interpolation for texturing. To avoid rendering artifacts, we must then ensure that the neighboring pixels have a
# sensible value
for p1, p2 in boundary_pairs:
    tex_closest_seed[p1] = tex_closest_seed[p2]

# Finally, fetch the color of each seed, and create the output image.
tex_output = seed_colors[tex_closest_seed]

# Display texture
plt.imshow(tex_output)
plt.show()

# Save the texture into the mesh object, and display the mesh and the seeds.
mesh.visual.material.image = PIL.Image.fromarray(np.uint8(tex_output*255))
seeds_visual = trimesh.points.PointCloud(seeds, color=[255, 0, 0, 255])

scene = trimesh.Scene([
    mesh,
    seeds_visual,
])

scene.show(smooth=False)
