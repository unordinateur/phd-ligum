import numpy as np


def normalize(v):
    return v / np.linalg.norm(v)


def perp(v):
    return -v[1], v[0]


def rot_vec(angle):
    return np.cos(angle), np.sin(angle)


def unit_slerp(u, v, t):
    theta = np.arccos(np.dot(u, v))
    return (np.sin((1 - t) * theta) * u + np.sin(t * theta) * v) / np.sin(theta)
