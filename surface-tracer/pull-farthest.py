import numpy as np
import trimesh.path.entities
import networkx as nx

from intersection import Intersection
from util import normalize, perp, rot_vec, unit_slerp


# Parameters
target_distance = 10
curl = 25   # in rad per distance

# Internal parameters
rad_per_step = 2 * np.pi / 1000
nudge_eps = 1e-3
step_size = np.inf if curl == 0 else np.abs(rad_per_step / curl)


# Load mesh
mesh = trimesh.load('bunny.off')
faces = mesh.faces
vertices = mesh.vertices
normals = mesh.face_normals
adjacency = nx.from_edgelist(mesh.face_adjacency)

# Precompute face basis and centers
faces_basis = np.empty((len(faces), 2, 3))
faces_centers = np.empty((len(faces), 3))
for i, face_vertices in enumerate(faces):
    xhat = normalize(np.diff(vertices[face_vertices[0:2]], axis=0))
    yhat = np.cross(normals[i], xhat)

    faces_basis[i] = [xhat, yhat]
    faces_centers[i] = np.mean(vertices[face_vertices], axis=0)

# Initial position and direction
cur_face = np.random.randint(len(faces) + 1)
cur_vertices = vertices[faces[cur_face]]
p = cur_vertices[0]
v = cur_vertices[1:] - p
c = np.random.rand(2)
c = c if c.sum() <= 1 else 1 - c
cur_pos = p + np.dot(c, v)
cur_dir = np.dot(rot_vec(np.random.rand() * 2 * np.pi), faces_basis[cur_face])

# Density information
distances_to_path = np.ones((len(faces)), dtype=np.float) * len(faces)
density = np.zeros((len(vertices)), dtype=np.float)
target_face = cur_face
bias_dir = cur_dir

# Draw geodesic path
path = [np.copy(cur_pos)]
distance = 0
while distance < target_distance:
    face_basis = faces_basis[cur_face]

    q = np.dot(face_basis, cur_pos)
    v = np.dot(face_basis, cur_dir)
    v_perp = perp(v)

    # Iterate over all neighbor faces; find the closest intersection
    inter = Intersection()

    for neighbor in adjacency.neighbors(cur_face):
        # Shared edge between both faces
        common_vertices = [vertices[v] for v in faces[neighbor] if v in faces[cur_face]]

        edge_pos = common_vertices[0]
        edge_vec = common_vertices[1] - edge_pos

        # Line - line intersection (projected on the face)
        # http://geomalgorithms.com/a05-_intersect-1.html
        p = np.dot(face_basis, edge_pos)
        u = np.dot(face_basis, edge_vec)

        w = p - q
        u_perp = perp(u)

        denom = np.dot(u_perp, v)
        t = np.dot(u_perp, w) / denom

        if 0 < t < inter.t:
            s = np.dot(v_perp, w) / denom
            inter = Intersection(neighbor, edge_pos, edge_vec, s, t)

    if inter.face == -1:
        print("Intersection failure")
        break

    cur_normal = normals[cur_face]

    # Update position.
    if inter.t > step_size:
        # The intersection is farther than the current step size: We stay on the face.
        step_d = step_size
        cur_pos += step_d * cur_dir

    else:
        # The intersection is closer than the step size: We walk up to the edge, and switch face.
        step_d = inter.t
        cur_pos = inter.position

        # Shift the intersection point towards the center of the new face for stability
        new_face = inter.face
        dcenter = faces_centers[new_face] - cur_pos
        cur_pos += nudge_eps * dcenter

        # Rotate the direction around the edge (Rodrigues' formula)
        new_normal = normals[new_face]
        edge_vec = inter.edge_vec
        cosangle = np.dot(cur_normal, new_normal)

        cur_dir = (cur_dir * cosangle
                   - cur_normal * np.dot(new_normal, cur_dir)
                   + new_normal * np.dot(cur_normal, cur_dir)
                   + edge_vec * np.dot(edge_vec, cur_dir) * (1 - cosangle) / np.dot(edge_vec, edge_vec))

        # Project direction onto new surface, and normalize, for numerical stability
        cur_dir -= new_normal * np.dot(new_normal, cur_dir)
        cur_dir /= np.linalg.norm(cur_dir)

        # Update face
        cur_face = new_face
        cur_normal = new_normal

        # Update coverage information. Only necessary if the face has never been visited before.
        if distances_to_path[cur_face] > 0:
            distances_from_cur = nx.single_source_dijkstra_path_length(adjacency, source=cur_face,
                                                                       cutoff=distances_to_path.max())
            # TODO: weights
            n = len(distances_from_cur)
            f = np.fromiter(distances_from_cur.keys(), dtype=np.int, count=n)
            d = np.fromiter(distances_from_cur.values(), dtype=np.float, count=n)
            distances_to_path[f] = np.minimum(d, distances_to_path[f])

        # Update the target face
        if cur_face == target_face:
            target_face = np.argmax(distances_to_path)

        # Update the bias direction
        path_to_farthest = nx.dijkstra_path(adjacency, source=cur_face, target=target_face)
        bias_dir = faces_centers[path_to_farthest[min(2, len(path_to_farthest) - 1)]] - faces_centers[cur_face]
        bias_dir -= cur_normal * np.dot(cur_normal, bias_dir)
        bias_dir /= np.linalg.norm(bias_dir)

    path.append(np.copy(cur_pos))
    distance += step_d

    # Add some curl to the direction
    # dir_basis = (cur_dir, np.cross(cur_dir, cur_normal))
    # cur_dir = np.dot(rot_vec(curl * step_d), dir_basis)

    # Bias direction
    costheta = np.clip(np.dot(cur_dir, bias_dir), -1, 1)
    dtheta = np.arccos(costheta)

    if dtheta > 0:
        t = min(1, (curl * step_d) / dtheta) * dtheta / np.pi
        cur_dir = unit_slerp(cur_dir, bias_dir, t)



# VISUALIZE RESULT
distances_to_path /= distances_to_path.max()
mesh.visual.face_colors = trimesh.visual.linear_color_map(distances_to_path, [[0, 255, 0, 255], [255, 0, 0, 255]])

path = np.array(path)

# Path3D with the path between the points
path_visual = trimesh.load_path(path)

# visualizable two points
points_visual = trimesh.points.PointCloud(path[[0, -1]], color=[0, 255, 0, 255])

# create a scene with the mesh, path, and points
scene = trimesh.Scene([
    #points_visual,
    path_visual,
    mesh,
])

scene.show(smooth=False)
