import numpy as np
from dataclasses import dataclass

@dataclass
class Intersection:
    face: int = -1
    edge_pos: np.ndarray = None
    edge_vec: np.ndarray = None

    s: float = np.inf
    t: float = np.inf

    @property
    def position(self):
        # For numerical stability, we compute the position of the intersection with the vectors defining the edge,
        # not the trajectory.
        return self.edge_pos + self.s * self.edge_vec
