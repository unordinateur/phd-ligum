The idea was to draw an straight (geodesic) continuous line along the surface of an object, but would cover the whole surface almost uniformly. We thus have to allow some deviation to a purely geodesic path to control the coverage.

I tried 2 approaches:
- "pull-farthest.py": We pick an initial point and direction at random on the model. We then start a geodesic walk along the model, but at each step, we bias the walk direction torwards the direction of the shortest path between the current position, and the point on the surface of the object the farthest to already drawn path.
- "djikstra-farthest.py": The approach is similar, but instead of doing geodesic walks, we walk along edges of the mesh. We pick a vertex at random on the mesh. We then find the vertex the farthest from any visited vertex, and use djikstra's algorithm to find the shortest path between both vertices, and add it to the output path. We then start the process again, taking as a starting position the end vertex of the previous step. This produces a much more jagged path, but it could then be smoothed by some post processing (not implemented).

Both programs take a ~1 minute to run, with no visible output until the end of the run. "intersection.py" and "util.py" are supporting files that are not executable. "bunny.off" is the 3D model.
