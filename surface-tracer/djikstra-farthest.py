import numpy as np
import trimesh.path.entities
import networkx as nx

# attach to logger so trimesh messages will be printed to console
# trimesh.util.attach_to_log()

# mesh objects can be loaded from a file name or from a buffer
mesh = trimesh.load('bunny.off')

vertices = mesh.vertices
edges = mesh.edges_unique
elengths = mesh.edges_unique_length

# alternative method for weighted graph creation
# you can also create the graph with from_edgelist and
# a list comprehension, which is like 1.5x faster
g = nx.from_edgelist((e[0], e[1], {'length': L}) for e, L in zip(edges, elengths))

# arbitrary indices of mesh.vertices to start with
nv = len(vertices)
cur_node = np.random.random_integers(nv)
distances_to_path = [nv]*nv

path = [cur_node]
targets = [cur_node]
for _ in range(1, 1000):
    paths_from_cur = nx.single_source_dijkstra_path(g,
                                                    source=cur_node,
                                                    cutoff=max(distances_to_path),
                                                    weight='length')

    for i, l in enumerate(distances_to_path):
        pfc = paths_from_cur[i]
        distances_to_path[i] = min(l, len(pfc))

    if targets[-1] == cur_node:
        index_longest = np.argmax(distances_to_path)
        targets.append(index_longest)

    path_to_target = paths_from_cur[targets[-1]]
    cur_node = path_to_target[1]
    path.append(cur_node)

longest_distance = max(distances_to_path)
distances_to_path = [d/longest_distance for d in distances_to_path]

# VISUALIZE RESULT
# make the sphere transparent-ish
mesh.visual.vertex_colors = trimesh.visual.linear_color_map(distances_to_path, [[0, 255, 0, 200], [255, 0, 0, 200]])
# Path3D with the path between the points
path_visual = trimesh.load_path(mesh.vertices[path])
# visualizable two points
points_visual = trimesh.points.PointCloud(mesh.vertices[targets])

# create a scene with the mesh, path, and points
scene = trimesh.Scene([
    points_visual,
    path_visual,
    mesh])

scene.show(smooth=False)
exit()








for i in range(len(mesh.faces)):
    mesh.visual.face_colors[i] = trimesh.visual.random_color()

points = np.array([[0, 0, 0], [0, 1, 0], [1, 1, 0]])
indices = [0,1,2]
path = trimesh.path.entities.Line(indices)
path = trimesh.path.Path3D([path], points)

scene = trimesh.Scene()
scene.add_geometry(mesh)
scene.add_geometry(path)
scene.show(smooth=False)
