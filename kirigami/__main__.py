import svgwrite

# Parameters
size = ('9in', '9in')
nx = 8
ny = 30
cut_ratio = 0.85

# Derived values
space_ratio = 1 - cut_ratio
width_per_cut = 1/(nx - space_ratio)
cut_width = width_per_cut * cut_ratio
row_spacing = 1/(ny - 1)

# Output object
dwg = svgwrite.Drawing('test.svg', size=size, profile='full')


def percent_string(s):
    return f'{s * 100}%'


# The cuts are organized in lines of alternating shift.
for i in range(ny):
	#For each line, find the amount of cuts to do.
    odd_row = (i % 2) == 1
    row_nx = nx + odd_row

    y = row_spacing * i
    y = pctstr(y)

	# For each cut, add a line in the output svg file.
    for j in range(row_nx):
        x1 = (j - (odd_row/2)) * width_per_cut
        x2 = x1 + cut_width

        x1 = max(x1, 0)
        x2 = min(x2, 1)

        dwg.add(dwg.line((pctstr(x1), y), (pctstr(x2), y), stroke='red'))

dwg.save()
