The idea was to investigate the properties of sheets of material when performing cuts in them.
For example: https://www.ncbi.nlm.nih.gov/pubmed/26574739

This code I wrote outputs a regular pattern of cuts in a format suitable to be cut by a laser cutter.
